js-clustering

Algoritmo de clusterização em JS, com renderização em HTML.

Para rodar o projeto:
1. npm install (para instalar as deéndências)
2. npm start (para compilar)
3. Após o projeto ser compilado é possível para o processo (ctrl + c), pois o webpack está aguardando mudanças
4. Abrir o arquivo index.html que está na pasta raiz do projeto