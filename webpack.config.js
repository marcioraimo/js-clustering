module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js'
  },
  watch: true,
  watchOptions: {
    ignored: ['node_modules', 'dist'],
    poll: 1000
  }
};
