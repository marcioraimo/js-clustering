import { Centroid, Chart, Serie, Point, } from './common';
import { COLORS, SHAPES } from './common/constants';
// import { SolidGaugePoint, SolidGaugeSerie } from './solidGauge';

const divName = 'chartRender'
var chart = null;
var base = null;
var series = [];
var centroids = [];
var points = [];
var maxCentroids = 2;
var maxPoints = 5;
// export function start() {
//     var serie = new SolidGaugeSerie('dinheiro gasto');
//     var point = new SolidGaugePoint(150, 100, 80, 'green');

//     var serie2 = new SolidGaugeSerie('dinheiro vivo');
//     var point2 = new SolidGaugePoint(360, 100, 80, 'red');

//     serie.addPoint(point);
//     serie2.addPoint(point2);
//     var chart = new Chart({
//         type: 'solidgauge',
//         renderTo: divName,
//         title: 'Opa',
//         yMax: 360
//     });
//     chart.addSerie(serie2);
//     chart.addSerie(serie);
// }

export function start() {
    base = new Serie('Base', 'gray');
    for (var index = 0; index < maxCentroids; index++) {
        var centroid = new Centroid({
            x: randomNumber(),
            y: randomNumber(),
            color: COLORS[randomNumber(1, COLORS.length)]
        });
        centroids.push(centroid);
        series.push(new Serie('Centroid ' + index, centroid.color));
        series[index].addPoint(centroid);
    }
    for (var index = 0; index < maxPoints; index++) {
        points.push(new Point(randomNumber(), randomNumber()));
    }
    base.addPoints(points);
    chart = new Chart({
        type: "scatter",
        renderTo: divName,
        title: 'teste'
    });
    chart.addSerie(base);
    chart.addSeries(series);
}

export function next() {
    centroids.forEach((centroid) => {
        centroid.clearPoints();
    });
    series.forEach((serie) => {
        serie.clearPoints();
    });
    chart.clearSeries();
    points.forEach((point) => {
        var valorMenor = 1000;
        var valorAtual = 0;
        var centroidPertencente = 0;
        centroids.forEach((centroid, index) => {
            valorAtual = euclideanDistance(point, centroid);
            if (valorAtual < valorMenor) {
                valorMenor = valorAtual;
                centroidPertencente = index;
            }
        });
        centroids[centroidPertencente].addPoint(point);
    });
    for (var index = 0; index < centroids.length; index++) {
        centroids[index].addPointsToSerie(series[index]);
        centroids[index].calculateMean();
        series[index].addPoint(centroids[index]);
    }
    chart.updateSeries(series);
    chart.update();
}

function randomNumber(min = 1, max = 100) {
    return Math.floor(Math.random() * max) + min;
}

function euclideanDistance(point, centroid) {
    const distance = Math.sqrt(Math.pow(point.x - centroid.x, 2) + Math.pow(point.y - centroid.y, 2));
    return distance;
}