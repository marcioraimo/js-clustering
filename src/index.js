import * as start from './start';

window.onload = () => {
    start.start();
    document.getElementById('next').addEventListener('click', start.next);
};
