export default class SolidGaugeSerie {
    /**
     * @param {string} name
     * @param {string} color
     * @param {Point[]} data
     */
    constructor(name, data = []) {
        if (typeof name !== 'string') {
            throw new Error(`Atributo 'nome' não informado ou com tipo incorreto`);
        }
        this.name = name;
        this.data = data;
        this.rounded = true;
        this.dataLabels = {
            enabled: false
        }
    }

    /**
     * Adiciona um ponto à Serie
     * @param {Point} point
     * @return void
     */
    addPoint(point) {
        this.data.push(point)
    }

    /**
     * Adiciona uma coleção de pontos para à Serie
     * @param {Point[]} points
     */
    addPoints(points) {
        points.forEach((point) => {
            this.data.push(point);
        });
    }
}