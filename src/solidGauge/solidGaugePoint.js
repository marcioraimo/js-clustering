export default class SolidGaugePoint {

    /**
     * @param {number} y
     * @param {number} outerRadius
     * @param {number} innerRadius
     * @param {string} color
     */
    constructor(y, outerRadius, innerRadius, color) {
        this.y = y;
        this.radius = `${outerRadius}%`;
        this.innerRadius = `${innerRadius}%`;
        this.color = color;
    }
}