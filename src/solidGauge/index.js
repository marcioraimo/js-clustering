import SolidGaugePoint from './solidGaugePoint';
import SolidGaugeSerie from './solidGaugeSerie';

export {
    SolidGaugePoint,
    SolidGaugeSerie
}