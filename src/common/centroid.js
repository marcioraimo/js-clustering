import Point from './point';

export default class Centroid extends Point {

    /**
     * @param {Object} params
     * @param {number} params.x
     * @param {number} params.y
     * @param {string} params.color
     * @param {number} [params.radius]
     * @param {string} [params.symbol]
     */
    constructor(params) {
        super(params.x, params.y);
        this.color = params.color;
        this.points = [];
        this.marker = {
            radius: params.radius || 8,
            symbol: params.symbol || 'diamond'
        }
    }

    addPoint(point) {
        this.points.push(point);
    }

    clearPoints() {
        this.points = [];
    }

    addPointsToSerie(serie) {
        serie.addPoints(this.points);
    }

    calculateMean() {
        var sumX = 0;
        var sumY = 0;
        this.points.forEach((point) => {
            sumX += point.x;
            sumY += point.y;
        });
        this.x = sumX/this.points.length;
        this.y = sumY/this.points.length;
        console.log('//////////**********')
        console.log('Centroid: ', this.color);
        console.log('X: ', this.x);
        console.log('Y: ', this.y);
        console.log('Points: ', this.points.length);
    }
}