import Highcharts from 'highcharts';
import HighchartsMore from 'highcharts/highcharts-more';
import SolidGauge from 'highcharts/modules/solid-gauge';
HighchartsMore(Highcharts);
SolidGauge(Highcharts);

var chart = null;

export default class Chart {

    /**
     * @param {ChartOptions} options 
     */
    constructor(options) {
        this.type = options.type;
        this.title = options.title;
        this.renderTo = options.renderTo;
        if (options.type === 'solidgauge') {
            chart = new Highcharts.Chart(options.renderTo, {
                chart: {
                    type: options.type
                },
                title: {
                    text: options.title
                },
                series: [],
                pane: {
                    startAngle: 270,
                    endAngle: 630,
                    background: [{
                        backgroundColor: 'white',
                        borderColor: 'white',
                        borderWidth: 0
                    }]
                },
                yAxis: {
                    min: 0,
                    max: options.yMax,
                    tickPositions: []
                }
            });
        } else {
            chart = new Highcharts.Chart(options.renderTo, {
                chart: {
                    type: options.type
                },
                title: {
                    text: options.title
                },
                series: []
            });
        }
    }

    get() {
        return chart;
    }

    addSerie(serie) {
        chart.addSeries(serie);
    }

    addSeries(series) {
        series.forEach((serie) => {
            chart.addSeries(serie)
        });
    }

    removeSerie(delSerie) {
        chart.series.forEach((serie, index) => {
            if (serie.name == delSerie.name) {
                chart.series[index].remove(true);
            }
        });
    }

    clearSeries() {
        const tamanho = chart.series.length;
        for(var index = 0; index < tamanho; index ++) {
            chart.series[0].remove(true)
        }
    }

    updateSeries(series) {
        series.forEach((serie) => {
            chart.addSeries(serie)
        });
    }

    update() {
        chart.redraw(true);
    }
}