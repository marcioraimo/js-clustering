declare class Chart {
    constructor(options: ChartOptions);
    renderTo: string;
    title: string;
    type: string;
    addSeries(serie: Serie);
    destroy();
    setTitle(text: string);
    showLoading(text: string);
    update();
}

declare interface ChartOptions {
    renderTo: string;
    title: string;
    type: 'area' | 'bar' | 'bubble' | 'gauge' | 'line' | 'pareto' | 'pie' | 'scatter';
    yMax?: number;
}

declare class Serie {
    constructor(options: SerieOptions);
    chart: Chart;
    color: string;
    label: string;
    addPoint(point: Point);
    remove(redraw: boolean = true);
    removePoint(index: number);
    update();
}

declare interface SerieOptions {
    chart: Chart;
    color: string;
    label: string;
}

declare class Point {
    constructor(options: PointOptions);
    x: number;
    y: number;
}

declare interface PointOptions {
    x: number;
    y: number;
}